const HtmlWebpackPlugin = require("html-webpack-plugin");
const AddAssetHtmlPlugin = require("add-asset-html-webpack-plugin");
const path = require("path");
const webpack = require("webpack");
const webpackMerge = require("webpack-merge");

const root = path.resolve(__dirname);
const dist = path.join(root, "dist");
const lib = path.join(root, "lib");

const entryMap = [
    ["polyfills", path.join(root, "src", "polyfills.ts")],
    ["vendor", path.join(root, "src", "vendor.ts")],
    ["app", path.join(root, "src", "index.ts")],
];
const entry = Object.assign({}, 
    ...entryMap.map(([key, value]) => ({[key]: value})));
const chunkSorter = (a, b) => {
    const order = Object.assign({}, 
        ...entryMap.map(([key, value], index) => ({[key]: index})));
    return order[a.names[0]] - order[b.names[0]];
}

var core = {
    devtool: "cheap-module-eval-source-map",
    //devtool: "source-map",
    devServer: {
        port: 9000
    },
    entry,
    output: {
        path: dist,
    },
    module: {
        rules: [
            {
                test: /\.ts$/,
                enforce: 'pre',
                loader: 'tslint-loader',
            },
        ],
    },
    plugins: [
        new webpack.DllReferencePlugin({
            context: '.',
            manifest: require(path.join(dist, 'polyfills-manifest.json')),
        }),
        new webpack.DllReferencePlugin({
            context: '.',
            manifest: require(path.join(dist, 'vendor-manifest.json')),
        }),
        new AddAssetHtmlPlugin([
            {filepath: path.join(dist, 'polyfills.dll.js'), includeSourcemap: false},
            {filepath: path.join(dist, 'vendor.dll.js'), includeSourcemap: false},
        ]),
    ],
    resolve: {
        extensions: [".ts", ".tsx", ".js"],
    },
    resolveLoader: {
        modules: [lib, "node_modules"],
        extensions: [".js", ".json"],
        mainFields: ["loader", "main"]
    }
};

var es2015 = webpackMerge(core, {
    module: {
        rules: [
            { 
                test: /\.tsx?$/, 
                use: [
                    // {loader: "peek-loader", options: {stage: "after"}},
                    {loader: "ts-loader"},
                    // {loader: "peek-loader", options: {stage: "before"}},
                ],
            },
        ],
    },
    output: {
        filename: "[name].es2015.js",
    },
    plugins: [
        new HtmlWebpackPlugin({
            filename: "index.html",
            template: path.join(root, "src", "index.ejs"),
            chunksSortMode: chunkSorter,
        }),
        new HtmlWebpackPlugin({
            filename: "index.es2015.html",
            template: path.join(root, "src", "index.ejs"),
            chunksSortMode: chunkSorter,
        }),
    ],
});

var commonjs = webpackMerge(core, {
    module: {
        rules: [
            { 
                test: /\.tsx?$/, 
                use: [
                    // {loader: "peek-loader", options: {stage: "after"}},
                    {
                        loader: "ts-loader",
                        options: {
                            configFileName: "tsconfig.commonjs.json",
                        }
                    },
                    // {loader: "peek-loader", options: {stage: "before"}},
                ]
            },
        ],
    },
    output: {
        filename: "[name].commonjs.js",
    },
    plugins: [
        new HtmlWebpackPlugin({
            filename: "index.commonjs.html",
            template: path.join(root, "src", "index.ejs"),
            chunksSortMode: chunkSorter,
        }),
    ],
});

module.exports = [es2015, commonjs];
